# Jogo da Velha
// Desenvolvimento do Jogo da Velha

Funcionalidades:
- Otimização da checagem de ganhador - 10
- Verificação de empate (mostra quando der empate) - 10
- Fim de partida (proibir outras jogadas) - 10
- Utilização de textos informativos na página (remoção dos alerts). - 10
- Implementar possibilidade de iniciar nova partida sem recarregar a página - 10

Sugestão: Contagem de vitórias de X e O (Implementar um placar de varias partidas - uso de localStore ) 
https://www.w3schools.com/html/html5_webstorage.asp

Visual:
- Uso de dois personagens rivais - 10
- Interface agradável visualmente (palheta de cores) - 20
- Interface tematizada de acordo com os personagens escolhidos - 20